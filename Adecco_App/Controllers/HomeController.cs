﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Adecco_App.Models;
namespace Adecco_App.Controllers
{
	public class HomeController : Controller
	{
		AdeccoEntities context;
		ServiceReference1.AdeccoServiceClient service = new ServiceReference1.AdeccoServiceClient();
		public HomeController() 
		{
			context = new AdeccoEntities();
		}
		public ActionResult Index()
		{
			
            var listWCF_User=service.GetAllUser().ToList();
            var lUser = new List<User>();
            foreach (var item in listWCF_User)
            {
                var user = new User();
                user.IdUser = item.IdUser;
                user.UserName = item.UserName;

                lUser.Add(user);
            }
            ViewData.Model = lUser;
			return View();
		}

		

		public ActionResult Add()
		{
			return View();
		}
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult Add(FormCollection form)
		{
            
			var newUser=new User();
			TryUpdateModel(newUser, new string[] { "UserName", "UserPsw" }, form.ToValueProvider());
			if (string.IsNullOrEmpty(newUser.UserName))
			{
				ModelState.AddModelError("UserName", "User is required");
			}
			if (string.IsNullOrEmpty(newUser.UserPsw))
			{
				ModelState.AddModelError("UserPsw", "Password is required");
			}
			if (ModelState.IsValid)
			{
                var _user = service.AddUser(newUser.UserName, newUser.UserPsw);   
				return RedirectToAction("Index");
			}
			 return View(newUser);
		}



		public ActionResult Edit(int id)
		{
			var userEdit = context.Set<User>().First(u => u.IdUser == id);
			ViewData.Model = userEdit;
			return View();
		}
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult Edit(FormCollection form)
		{
			var id = Convert.ToInt32(form["Id"]);

            if (id < 1 )
            {
                return HttpNotFound();
            }
            var userUpdate = context.Set<User>().First(u => u.IdUser == id);
			TryUpdateModel(userUpdate, new string[] { "UserName", "UserPsw" }, form.ToValueProvider());
			if (string.IsNullOrEmpty(userUpdate.UserName))
			{
				ModelState.AddModelError("UserName", "User is required");
			}
			if (string.IsNullOrEmpty(userUpdate.UserPsw))
			{
				ModelState.AddModelError("UserPsw", "Password is required");
			}
			if (ModelState.IsValid)
			{
                var _user = service.UpdateUser(id, userUpdate.UserName, userUpdate.UserPsw);
                //context.SaveChanges();
				return RedirectToAction("Index");
			}
			return View(userUpdate);
		   
		}

        public ActionResult Delete(int id)
        {
            var userDel = context.Set<User>().First(u => u.IdUser == id);
            ViewData.Model = userDel;

            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(FormCollection form)
        {
            var id = Convert.ToInt32(form["id"]);
            if (id<1)
            {
                return HttpNotFound();
            }
            //var userUpdate = context.Set<User>().First(u => u.IdUser == id);
            //TryUpdateModel(userUpdate, new string[] { "UserName", "UserPsw" }, form.ToValueProvider());
            //if (string.IsNullOrEmpty(userUpdate.UserName))
            //{
            //    ModelState.AddModelError("UserName", "User is required");
            //}
            //if (string.IsNullOrEmpty(userUpdate.UserPsw))
            //{
            //    ModelState.AddModelError("UserPsw", "Password is required");
            //}
            if (ModelState.IsValid)
            {
              
                var _user = service.DeleteUserById(id);              
                return RedirectToAction("Index");
            }
            return View("Index");

        }
    }
}