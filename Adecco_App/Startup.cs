﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Adecco_App.Startup))]
namespace Adecco_App
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
