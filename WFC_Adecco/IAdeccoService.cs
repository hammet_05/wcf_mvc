﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Adecco_App;
using Adecco_App.Models;
namespace WFC_Adecco
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IAdeccoService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IAdeccoService
    {
        [OperationContract]
        List<User> GetAllUser();

        [OperationContract]
        User GetAllUserById(int id);

        [OperationContract]
        int DeleteUserById(int Id);
        [OperationContract]
        int AddUser(string userName, string password);
        [OperationContract]
        int UpdateUser(int Id, string userName, string password);

    }
    [DataContract]
    public class User
    {
        [DataMember]
        public int IdUser { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Password { get; set; }
    }
}
