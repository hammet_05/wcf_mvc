﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Adecco_App.Models;
using System.Data.Entity;
namespace WFC_Adecco
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "AdeccoService" en el código y en el archivo de configuración a la vez.
    public class AdeccoService : IAdeccoService
    {
        public List<User> GetAllUser()
        {
            var lUsers = new List<User>();
            var context = new AdeccoEntities();
            var listUsers = (from u in context.Users select u).ToList();
            foreach (var item in listUsers)
            {
                var user = new User();
                user.IdUser = item.IdUser;
                user.UserName = item.UserName;
                lUsers.Add(user);

            }

            return lUsers;
        }
        public User GetAllUserById(int id)
        {
            var context = new AdeccoEntities();
            var lUser = (from u in context.Users where u.IdUser == id select u).ToList();
            User usr = new User();
            foreach (var item in lUser)
            {
                usr.IdUser = item.IdUser;
                usr.UserName = item.UserName;
            }

            return usr;
        }
        public int DeleteUserById(int Id)
        {

            var context = new AdeccoEntities();
            var user = new Adecco_App.Models.User();
            user.IdUser = Id;
            context.Entry(user).State = EntityState.Deleted;
            int retValue = context.SaveChanges();
            return retValue;
        }
        public int AddUser(string userName, string password)
        {
            var context = new AdeccoEntities();
            var user = new Adecco_App.Models.User();
            user.UserName = userName;
            user.UserPsw = password;
            context.Users.Add(user);
            int Retval = context.SaveChanges();
            return Retval;
        }
        public int UpdateUser(int Id, string userName, string password)
        {
            try
            {
                var context = new AdeccoEntities();              
                var user = new Adecco_App.Models.User();
                user.IdUser = Id;
                user.UserName = userName;
                user.UserPsw = password;
                context.Entry(user).State = EntityState.Modified;

                int retVal = context.SaveChanges();
                return retVal;
            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }
    }
  
}
